angular.module('tickerApp.services',[])

// .constant('FIREBASE_URL','https://project-9128587856344331241.firebaseio.com')

.factory('firebaseRef', function($firebase, FIREBASE_URL) {

  // var firebaseRef = new Firebase(FIREBASE_URL);
  //
  // return firebaseRef;
})

.factory('firebaseUserRef', function(firebaseRef) {

  // var userRef = firebaseRef.child('users');
  //
  // return userRef;
})

.factory('userService', function(modalService) {

  var login = function(user, signup) {
    // modalService.closeModal();
    // firebaseRef.authWithPassword({
    //   email    : user.email,
    //   password : user.password
    // }, function(error, authData) {
    //   if (error) {
    //     console.log("Login Failed!", error);
    //   } else {
    //     $rootScope.currentUser = user;
    //
    //     if (signup) {
    //       modalService.closeModal();
    //     } else {
    //
    //       modalService.closeModal();
    //       $timeout(function() {
    //         $window.location.reload(true);
    //       }, 400);
    //     }
    //   }
    // });
  };

  var signup = function(user) {
    // modalService.closeModal();
    // firebaseRef.createUser({
    //   email    : user.email,
    //   password : user.password
    // }, function(error, userData) {
    //   if (error) {
    //     console.log("Error creating user:", error);
    //   } else {
    //     login(user, true);
    //   }
    // });
  };

  var logout = function() {
    // firebaseRef.unauth();
    // $window.location.reload(true);

  };

  var updateStocks = function(stocks) {
    // firebaseUserRef.child(getUser().uid).child('stocks').set(stocks);
  };

  var updateNotes = function(ticker, notes) {
    // firebaseUserRef.child(getUser().uid).child('notes').child(ticker).remove();
    // notes.forEach(function(note) {
    //   firebaseUserRef.child(getUser().uid).child('notes').child(note.ticker).push(note);
    // });
  };

  var loadUserData = function(authData) {

    // firebaseUserRef.child(authData.uid).child('stocks').once('value', function(snapshot) {
    //
    //   var stocksFromDatabase = [];
    //   snapshot.val().forEach(function(stock) {
    //     var stockToAdd = {ticker: stock.ticker};
    //     stocksFromDatabase.push(stockToAdd);
    //   });
    //
    //   myStocksCacheService.put('myStocks', stocksFromDatabase);
    // },
    // function(error) {
    //   console.log("Firebase error -> stocks" + err);
    // });
    //
    // firebaseUserRef.child(authData.uid).child('notes').once('value', function(snapshot) {
    //
    //   snapshot.forEach(function(stockWithNotes) {
    //     var notesFromDatabase = [];
    //
    //     stockWithNotes.forEach(function(note) {
    //       notesFromDatabase.push(note.val());
    //       var cacheKey = note.child('ticker').val();
    //       notesCacheService.put(cacheKey, notesFromDatabase);
    //     });
    //   });
    // },
    // function(error) {
    //   console.log("Firebase error -> notes" + err);
    // });
  };

  var getUser = function() {
    // return firebaseRef.getAuth();
  };

  // if (getUser()) {
  //   $rootScope.currentUser = getUser();
  // }

  return {
    login: login,
    signup: signup,
    logout: logout,
    updateStocks: updateStocks,
    getUser: getUser,
    updateNotes: updateNotes
  };
})

.factory('dateService', function($filter){

  var currentDate = function(){
    var d = new Date();
    var date = $filter('date')(d,'yyyy-MM-dd');
    return date;
  };

  var oneYearAgoDate = function(){
    var d = new Date(new Date().setDate(new Date().getDate()-365));
    var date = $filter('date')(d,'yyyy-MM-dd');
    return date;
  };

  return {
    currentDate: currentDate,
    oneYearAgoDate: oneYearAgoDate
  };
})


.service('modalService', function($ionicModal) {

  this.openModal = function(id) {

    var _this = this;

    if (id == 1) {
      // Create the search modal
      $ionicModal.fromTemplateUrl('templates/search.html', {
        scope: null,
        controller: 'SearchCtrl'
      }).then(function(modal) {
        _this.modal = modal;
        _this.modal.show();
      });
    } else if (id == 2) {
      $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: null,
        controller: 'LoginCtrl'
      }).then(function(modal) {
        _this.modal = modal;
        _this.modal.show();
      });
    } else if (id == 3) {
      $ionicModal.fromTemplateUrl('templates/signup.html', {
        scope: null,
        controller: 'SignupCtrl'
      }).then(function(modal) {
        _this.modal = modal;
        _this.modal.show();
      });
    }
  };

  this.closeModal = function() {

    var _this = this;

    if (!_this.modal) return;
    _this.modal.hide();
    _this.modal.remove();
  };
})
///////////////////////////
// caching services
/////////////////////////////
.factory('chartDataCacheService', function(CacheFactory) {
  var chartDataCache;

  if(!CacheFactory.get('chartDataCache')){
    chartDataCache = CacheFactory('chartDataCache', {
      maxAge: 60 * 60 * 8 * 1000, //1 day
      deleteOnExpire: 'aggressive',
      storageMode: 'localStorage'
    });
  }
  else{
    chartDataCache = CacheFactory.get('chartDataCache');
  }

  return chartDataCache;
})

.factory('stockDetailsCacheService', function(CacheFactory) {
  var stockDetailsCache;

  if(!CacheFactory.get('stockDetailsCache')){
    stockDetailsCache = CacheFactory('stockDetailsCache', {
      maxAge: 60 * 1000, // 1 minute
      deleteOnExpire: 'aggressive',
      storageMode: 'localStorage'
    });
  }
  else{
    stockDetailsCache = CacheFactory.get('stockDetailsCache');
  }

  return stockDetailsCache;
})

.factory('stockPriceCacheService', function(CacheFactory) {
  var stockPriceCache;

  if(!CacheFactory.get('stockPriceCache')){
    stockPriceCache = CacheFactory('stockPriceCache', {
      maxAge: 10 * 1000, // 10 seconds
      deleteOnExpire: 'aggressive',
      storageMode: 'localStorage'
    });
  }
  else{
    stockPriceCache = CacheFactory.get('stockPriceCache');
  }

  return stockPriceCache;
})

.factory('notesCacheService', function(CacheFactory) {
  var notesCache;

  if(!CacheFactory.get('notesCache')){
    notesCache = CacheFactory('notesCache', {
      storageMode: 'localStorage'
    });
  }
  else{
    notesCache = CacheFactory.get('notesCache');
  }

  return notesCache;
})

.factory('comIDCacheService', function(CacheFactory) {
  var comIDCache;

  if(!CacheFactory.get('comIDCache')){
    comIDCache = CacheFactory('comIDCache', {
      storageMode: 'localStorage'
    });
  }
  else{
    comIDCache = CacheFactory.get('comIDCache');
  }

  return comIDCache;
})

.factory('fillMyStocksCacheService', function(CacheFactory){

  var myStocksCache;

  if(!CacheFactory.get('myStocksCache')){
    myStocksCache = CacheFactory('myStocksCache', {
      storageMode: 'localStorage'
    });
  }else{
    myStocksCache = CacheFactory.get('myStocksCache');
  }

  var fillMyStocksCache = function(){
    var myStocksArray = [
      {ticker:"ABS"},
      {ticker:"DD"},
      {ticker:"BDO"},
    ];

    myStocksCache.put('myStocks', myStocksArray);
  };

  return {
    fillMyStocksCache: fillMyStocksCache
  };

})

.factory('myStocksCacheService', function(CacheFactory){

  var myStocksCache = CacheFactory.get('myStocksCache');

  return myStocksCache;

})

.factory('myStocksArrayService', function(fillMyStocksCacheService, myStocksCacheService){

  if(!myStocksCacheService.info('myStocks')){
    fillMyStocksCacheService.fillMyStocksCache();
  }

  var myStocks = myStocksCacheService.get('myStocks');
  console.log(myStocks);
  return myStocks;
})

.factory('followStockService', function(myStocksArrayService, myStocksCacheService){

  return {
    follow: function(ticker){
      var stockToAdd = {"ticker":ticker};

      myStocksArrayService.push(stockToAdd);
      myStocksCacheService.put('myStocks', myStocksArrayService);
    },

    unfollow: function(ticker){

      for (var i = 0; i < myStocksArrayService.length; i++) {
        if (myStocksArrayService[i].ticker == ticker) {
          myStocksArrayService.splice(i,i);
          myStocksCacheService.remove('myStocks');
          myStocksCacheService.put('myStocks', myStocksArrayService);

          break;
        }
      }
    },

    checkFollowing: function(ticker){

      for (var i = 0; i < myStocksArrayService.length; i++){
        if(myStocksArrayService[i].ticker == ticker){
          return true;
        }
      }

      return false;
    }
  };

})

///////////////////////////
// Stock API services
/////////////////////////////
.factory('stockDataService', function($q, $http, stockDetailsCacheService, stockPriceCacheService, comIDCacheService){

  var getDetailsData = function(ticker){
    console.log(ticker);
    console.log(comIDCacheService.get(ticker));
    var comID = comIDCacheService.get(ticker).comID;
    var secID = comIDCacheService.get(ticker).secID;
    var deferred = $q.defer(),

    cacheKey = ticker,
    stockDetailsCache = stockDetailsCacheService.get(cacheKey),

    url = "http://www.pse.com.ph/stockMarket/companyInfo.html?method=fetchHeaderData&ajax=true&company="+comID+"&security="+secID;
    if(stockDetailsCache){
        deferred.resolve(stockDetailsCache);
    }else{
        $http.get(url)
          .success(function(json){
            var jsonData = json.records[0];
            // console.log(json);
            deferred.resolve(jsonData);
            stockDetailsCacheService.put(cacheKey, jsonData);
          })
          .error(function(error){
            console.log("Details data error: "+error);
          });
    }

    return deferred.promise;
  };

  var getPriceData = function(ticker){
    var deferred = $q.defer(),
    cacheKey = ticker,
    stockPriceCache = stockPriceCacheService.get(cacheKey),

    url = "http://phisix-api.appspot.com/stocks/"+ ticker +".json";

    if(stockPriceCache){
      deferred.resolve(stockPriceCache);
    }else{
      $http.get(url)
        .success(function(json){
          var jsonData = json;
          deferred.resolve(jsonData);
          stockPriceCacheService.put(cacheKey, jsonData);
        })
        .error(function(error){
          console.log("Price data error: "+error);
        });
    }

    return deferred.promise;

  };

  return {
    getPriceData: getPriceData,
    getDetailsData: getDetailsData
  };

})

.factory('chartDataService', function($q, $http, chartDataCacheService, comIDCacheService) {

  var getHistoricalData = function(ticker, fromDate, todayDate) {

    var comID = comIDCacheService.get(ticker).comID;
    var secID = comIDCacheService.get(ticker).secID;
    var deferred = $q.defer(),

    cacheKey = ticker,
    chartDataCache = chartDataCacheService.get(cacheKey),

    url ="http://www.pse.com.ph/stockMarket/companyInfoHistoricalData.html?method=getRecentSecurityQuoteData&security="+secID+"&company="+comID+"&ajax=true";

    if(chartDataCache){
      deferred.resolve(chartDataCache);
    }else{
      $http.get(url)
        .success(function(json) {
          var jsonData = json.records;

          var priceData = [],
            volumeData = [];

          jsonData.forEach(function(dayDataObject) {

            var dateToMills = dayDataObject.tradingDate,
              date = Date.parse(dateToMills),
              price = parseFloat(Math.round(dayDataObject.sqClose * 100) / 100).toFixed(3),
              volume = dayDataObject.totalVolume,
              volumeDatum = '[' + date + ',' + volume + ']',
              priceDatum = '[' + date + ',' + price + ']';


            volumeData.unshift(volumeDatum);
            priceData.unshift(priceDatum);
          });

          var formattedChartData =
          '[{' +
              '"key":' + '"volume",' +
              '"bar":' + 'true,' +
              '"values":' + '[' + volumeData + ']' +
          '},' +
          '{' +
              '"key":' + '"' + ticker + '",' +
              '"values":' + '[' + priceData + ']' +
          '}]';

          deferred.resolve(formattedChartData);
          chartDataCacheService.put(cacheKey, formattedChartData);
        })
        .error(function(error) {
          console.log("Chart data error: " + error);
          deferred.reject();
        });
    }

    return deferred.promise;
  };

  return {
    getHistoricalData: getHistoricalData
  };
})

.factory('notesService', function(notesCacheService){

  return {

    getNotes: function(ticker){
      return notesCacheService.get(ticker);
    },

    addNote: function(ticker, note){

      var stockNotes = [];

      if(notesCacheService.get(ticker)){
        stockNotes = notesCacheService.get(ticker);
        stockNotes.push(note);
      }else{
        stockNotes.push(note);
      }
      notesCacheService.put(ticker, stockNotes);
    },

    deleteNote: function(ticker, index){

      var stockNotes = [];

      stockNotes = notesCacheService.get(ticker);
      stockNotes.splice(index,1);
      notesCacheService.put(ticker, stockNotes);

    }
  };
})

.factory('comIDService', function(comIDCacheService){

  return {

    populateCache: function(){
      var stocksID = {
        '2GO':{'comID':'29','secID':'146'},
        'AB':{'comID':'19','secID':'181'},
        'ABA':{'comID':'174','secID':'173'},
        'ABG':{'comID':'176','secID':'350'},
        'ABS':{'comID':'114','secID':'123'},
        'ABSP':{'comID':'15','secID':'259'},
        'AC':{'comID':'57','secID':'180'},
        'ACR':{'comID':'121','secID':'170'},
        'AEV':{'comID':'16','secID':'183'},
        'AGF':{'comID':'643','secID':'586'},
        'AGI':{'comID':'212','secID':'179'},
        'ALI':{'comID':'180','secID':'293'},
        'ALT':{'comID':'620','secID':'548'},
        'ANI':{'comID':'619','secID':'547'},
        'ANS':{'comID':'14','secID':'169'},
        'AP':{'comID':'609','secID':'532'},
        'APC':{'comID':'177','secID':'182'},
        'APO':{'comID':'52','secID':'171'},
        'APX':{'comID':'178','secID':'318'},
        'AR':{'comID':'33','secID':'321'},
        'ARA':{'comID':'38','secID':'294'},
        'AT':{'comID':'34','secID':'320'},
        'ATN':{'comID':'56','secID':'176'},
        'ATNB':{'comID':'56','secID':'175'},
        'AUB':{'comID':'641','secID':'584'},
        'BC':{'comID':'108','secID':'353'},
        'BCB':{'comID':'108','secID':'322'},
        'BCOR':{'comID':'9','secID':'391'},
        'BDO':{'comID':'260','secID':'468'},
        'BEL':{'comID':'21','secID':'252'},
        'BHI':{'comID':'63','secID':'187'},
        'BLOOM':{'comID':'49','secID':'233'},
        'BPI':{'comID':'234','secID':'101'},
        'BRN':{'comID':'13','secID':'234'},
        'BSC':{'comID':'60','secID':'188'},
        'CAL':{'comID':'636','secID':'576'},
        'CDC':{'comID':'39','secID':'297'},
        'CEB':{'comID':'624','secID':'560'},
        'CEI':{'comID':'186','secID':'191'},
        'CHI':{'comID':'110','secID':'298'},
        'CHIB':{'comID':'184','secID':'104'},
        'CIC':{'comID':'648','secID':'591'},
        'CIP':{'comID':'22','secID':'367'},
        'CNPF':{'comID':'652','secID':'597'},
        'COAL':{'comID':'637','secID':'580'},
        'COL':{'comID':'601','secID':'522'},
        'COSCO':{'comID':'50','secID':'178'},
        'CPG':{'comID':'189','secID':'219'},
        'CPM':{'comID':'621','secID':'549'},
        'CROWN':{'comID':'186','secID':'191'},
        'CYBR':{'comID':'67','secID':'295'},
        'DAVIN':{'comID':'2','secID':'150'},
        'DD':{'comID':'651','secID':'596'},
        'DFNN':{'comID':'187','secID':'125'},
        'DIZ':{'comID':'68','secID':'323'},
        'DMC':{'comID':'188','secID':'192'},
        'DMPL':{'comID':'642','secID':'585'},
        'DNL':{'comID':'639','secID':'582'},
        'DWC':{'comID':'647','secID':'590'},
        'ECP':{'comID':'70','secID':'126'},
        'EDC':{'comID':'603','secID':'525'},
        'EEI':{'comID':'71','secID':'148'},
        'EG':{'comID':'623','secID':'552'},
        'ELI':{'comID':'190','secID':'300'},
        'EMP':{'comID':'632','secID':'571'},
        'EVER':{'comID':'191','secID':'193'},
        'EW':{'comID':'634','secID':'573'},
        'FDC':{'comID':'75','secID':'199'},
        'FFI':{'comID':'196','secID':'117'},
        'FGEN':{'comID':'600','secID':'521'},
        'FGENF':{'comID':'600','secID':'568'},
        'FLI':{'comID':'226','secID':'302'},
        'FMETF':{'comID':'649','secID':'592'},
        'FNI':{'comID':'224','secID':'190'},
        'FOOD':{'comID':'602','secID':'524'},
        'FPH':{'comID':'197','secID':'197'},
        'FPI':{'comID':'220','secID':'200'},
        'GEO':{'comID':'198','secID':'201'},
        'GERI':{'comID':'193','secID':'307'},
        'GLO':{'comID':'69','secID':'127'},
        'GLOPP':{'comID':'69','secID':'598'},
        'GMA7':{'comID':'610','secID':'533'},
        'GSMI':{'comID':'94','secID':'160'},
        'GTCAP':{'comID':'633','secID':'572'},
        'HLCM':{'comID':'211','secID':'155'},
        'HOUSE':{'comID':'82','secID':'203'},
        'I':{'comID':'613','secID':'538'},
        'ICT':{'comID':'83','secID':'142'},
        'IDC':{'comID':'660','secID':'617'},
        'IMI':{'comID':'622','secID':'551'},
        'IMP':{'comID':'201','secID':'336'},
        'IMPB':{'comID':'201','secID':'375'},
        'ION':{'comID':'203','secID':'204'},
        'IS':{'comID':'204','secID':'128'},
        'ISM':{'comID':'36','secID':'324'},
        'JFC':{'comID':'86','secID':'158'},
        'JGS':{'comID':'210','secID':'207'},
        'LBC':{'comID':'236','secID':'469'},
        'LC':{'comID':'98','secID':'326'},
        'LCB':{'comID':'98','secID':'327'},
        'LFM':{'comID':'227','secID':'159'},
        'LIB':{'comID':'27','secID':'129'},
        'LIHC':{'comID':'37','secID':'377'},
        'LMG':{'comID':'205','secID':'379'},
        'LOTO':{'comID':'605','secID':'528'},
        'LPZ':{'comID':'61','secID':'186'},
        'LR':{'comID':'96','secID':'253'},
        'LRW':{'comID':'96','secID':'594'},
        'LTG':{'comID':'12','secID':'225'},
        'MA':{'comID':'119','secID':'328'},
        'MAB':{'comID':'119','secID':'329'},
        'MARC':{'comID':'175','secID':'177'},
        'MAXS':{'comID':'135','secID':'163'},
        'MBC':{'comID':'117','secID':'382'},
        'MBT':{'comID':'128','secID':'108'},
        'MCP':{'comID':'202','secID':'237'},
        'MED':{'comID':'126','secID':'383'},
        'MEG':{'comID':'127','secID':'215'},
        'MER':{'comID':'118','secID':'137'},
        'MFC':{'comID':'120','secID':'120'},
        'MFIN':{'comID':'263','secID':'503'},
        'MJIC':{'comID':'24','secID':'194'},
        'MPI':{'comID':'604','secID':'526'},
        'MRC':{'comID':'131','secID':'309'},
        'MRSGI':{'comID':'659','secID':'616'},
        'MVC':{'comID':'100','secID':'242'},
        'MWC':{'comID':'270','secID':'518'},
        'MWIDE':{'comID':'627','secID':'564'},
        'NI':{'comID':'103','secID':'213'},
        'NIKL':{'comID':'625','secID':'561'},
        'NOW':{'comID':'264','secID':'510'},
        'NRCP':{'comID':'606','secID':'529'},
        'OPM':{'comID':'43','secID':'338'},
        'OPMB':{'comID':'43','secID':'339'},
        'ORE':{'comID':'616','secID':'541'},
        'OV':{'comID':'45','secID':'341'},
        'PA':{'comID':'109','secID':'342'},
        'PAL':{'comID':'20','secID':'189'},
        'PAX':{'comID':'194','secID':'195'},
        'PBB':{'comID':'640','secID':'583'},
        'PCOR':{'comID':'136','secID':'139'},
        'PERC':{'comID':'578','secID':'516'},
        'PF':{'comID':'151','secID':'161'},
        'PFP2':{'comID':'151','secID':'610'},
        'PGOLD':{'comID':'629','secID':'567'},
        'PHA':{'comID':'148','secID':'258'},
        'PHN':{'comID':'107','secID':'184'},
        'PIP':{'comID':'617','secID':'543'},
        'PLC':{'comID':'158','secID':'344'},
        'PMPC':{'comID':'104','secID':'239'},
        'PNB':{'comID':'139','secID':'109'},
        'PNX':{'comID':'608','secID':'531'},
        'POPI':{'comID':'26','secID':'202'},
        'PPC':{'comID':'150','secID':'246'},
        'PRC':{'comID':'141','secID':'257'},
        'PRMX':{'comID':'214','secID':'419'},
        'PSB':{'comID':'142','secID':'112'},
        'PSE':{'comID':'478','secID':'512'},
        'PSPC':{'comID':'655','secID':'605'},
        'PX':{'comID':'137','secID':'331'},
        'PXP':{'comID':'628','secID':'566'},
        'RCB':{'comID':'232','secID':'113'},
        'REG':{'comID':'153','secID':'153'},
        'RFM':{'comID':'77','secID':'164'},
        'RLC':{'comID':'195','secID':'312'},
        'RLT':{'comID':'40','secID':'313'},
        'ROCK':{'comID':'635','secID':'574'},
        'RRHI':{'comID':'646','secID':'589'},
        'RWM':{'comID':'645','secID':'588'},
        'SBS':{'comID':'658','secID':'612'},
        'SCC':{'comID':'157','secID':'396'},
        'SECB':{'comID':'32','secID':'114'},
        'SEVN':{'comID':'143','secID':'251'},
        'SFI':{'comID':'165','secID':'402'},
        'SFIP':{'comID':'165','secID':'455'},
        'SGI':{'comID':'160','secID':'224'},
        'SLF':{'comID':'78','secID':'122'},
        'SLI':{'comID':'41','secID':'417'},
        'SM':{'comID':'599','secID':'520'},
        'SMC':{'comID':'154','secID':'165'},
        'SMC2C':{'comID':'154','secID':'579'},
        'SMC2D':{'comID':'154','secID':'613'},
        'SMC2E':{'comID':'154','secID':'614'},
        'SMC2F':{'comID':'154','secID':'615'},
        'SMC2G':{'comID':'154','secID':'621'},
        'SMC2H':{'comID':'154','secID':'622'},
        'SMPH':{'comID':'112','secID':'314'},
        'SOC':{'comID':'161','secID':'345'},
        'SPH':{'comID':'614','secID':'539'},
        'SSI':{'comID':'654','secID':'604'},
        'STI':{'comID':'222','secID':'376'},
        'SUN':{'comID':'73','secID':'254'},
        'T':{'comID':'163','secID':'418'},
        'TA':{'comID':'233','secID':'140'},
        'TAPET':{'comID':'653','secID':'599'},
        'TECH':{'comID':'630','secID':'569'},
        'TEL':{'comID':'6','secID':'134'},
        'TFC':{'comID':'8','secID':'413'},
        'TFHI':{'comID':'650','secID':'595'},
        'TUGS':{'comID':'644','secID':'587'},
        'UBP':{'comID':'167','secID':'115'},
        'UNI':{'comID':'92','secID':'227'},
        'UPM':{'comID':'168','secID':'334'},
        'URC':{'comID':'124','secID':'167'},
        'VITA':{'comID':'28','secID':'168'},
        'VLL':{'comID':'607','secID':'530'},
        'VMC':{'comID':'123','secID':'415'},
        'VUL':{'comID':'46','secID':'346'},
        'WEB':{'comID':'122','secID':'136'},
        'WIN':{'comID':'90','secID':'229'},
        'X':{'comID':'656','secID':'607'},
        'YEHEY':{'comID':'638','secID':'581'},
        'ZHI':{'comID':'89','secID':'232'}
      };

      keys = Object.keys(stocksID);

      for(i = 0; i < keys.length; i++){
        comIDCacheService.put(keys[i], stocksID[keys[i]]);
        // console.log(keys[i] + "=" + stocksID[keys[i]].comID +" ,secID: "+stocksID[keys[i]].secID);   // p1=value1, p2=value2, p3=value3
      }

    },

    getIDs: function(ticker){
      return comIDCacheService.get(ticker);
    },

    addID: function(ticker, comID, secID){
      var ids = {"comID":comID, "secID":secID};

      if(!comIDCacheService.get(ticker)){
        comIDCacheService.put(ticker, ids);
      }
    },

    deleteID: function(ticker, index){

    }
  };
})

.factory('newsService', function($q, $http){

  return {

    getNews: function(query){

      var deferred = $q.defer(),

      x2js = new X2JS(),
      // url = "http://finance.yahoo.com/rss/headline?s="+ticker;
      url = "http://news.google.com/news?pz=1&cf=all&ned=ph&hl=en&q="+ query +"+finance+stock&cf=all&output=rss";
      // http://finance.yahoo.com/rss/headline?s=yhoo

      $http.get(url)
        .success(function(xml){
          // console.log(xml);
          var xmlDoc = x2js.parseXmlString(xml),
           json = x2js.xml2json(xmlDoc),
           jsonData = json.rss.channel.item;
          deferred.resolve(jsonData);
        })
        .error(function(error){
          deferred.reject();
          console.log("News error: "+error);
        });

      return deferred.promise;
    }
  };

})

.factory('searchService', function($q, $http, comIDCacheService) {

  return {

    search: function(query) {

        var deferred = $q.defer(),

          url = 'http://www.pse.com.ph/stockMarket/home.html?method=findSecurityOrCompany&ajax=true&start=0&limit=10&query='+query;

          $http.get(url)
            .success(function(json){
              var jsonData = json.records;
              //only return stocks with ids in cache

              for (var i = 0; i < jsonData.length; i++) {
                if(!comIDCacheService.get(jsonData[i].symbol)){
                  var ids = {"comID":jsonData[i].listedCompany_companyId,"secID":jsonData[i].securityId};
                  comIDCacheService.put(jsonData[i].symbol, ids);
                }
              }

              deferred.resolve(jsonData);
            })
            .error(function(error){
              console.log("News feed error: "+error);
            });

          return deferred.promise;
    }
  };
})

;
