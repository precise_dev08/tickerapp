angular.module('tickerApp', [
  'ionic',
  'ngCordova',
  // 'firebase',
  'admobModule',
  'angular-cache',
  'nvd3',
  'nvChart',
  'cb.x2js',
  'tickerApp.controllers',
  'tickerApp.services',
  'tickerApp.directives',
  'tickerApp.filters'
])

.run(function($ionicPlatform, $rootScope,admobSvc) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      // StatusBar.styleHex("#ffffff");
    }
    
    admobSvc.setOptions({
        publisherId: "ca-app-pub-9822971532922573/1006311842",  // Required

    });

    admobSvc.createBannerView();
    // Handle events:
    $rootScope.$on(admobSvc.events.onAdOpened, function onAdOpened(evt, e) {
      console.log('adOpened: type of ad:' + e.adType);
    });


  });
})

.config(function($stateProvider, $urlRouterProvider) {


  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.myStocks', {
    url: '/my-stocks',
    views: {
      'menuContent': {
        templateUrl: 'templates/my-stocks.html',
        controller: 'MyStocksCtrl'
      }
    }
  })

  .state('app.stock', {
    url: '/:stockTicker',
    views: {
      'menuContent': {
        templateUrl: 'templates/stock.html',
        controller: 'StockCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/my-stocks');
});
