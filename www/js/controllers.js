angular.module('tickerApp.controllers', [])

.controller('AppCtrl', ['$scope', 'modalService','comIDService',
  function($scope, modalService, comIDService){

    $scope.modalService = modalService;
    comIDService.populateCache();

    $scope.logout = function() {
      userService.logout();
    };
}])

.controller('SearchCtrl', ['$scope', '$state', 'modalService', 'searchService',
  function($scope, $state, modalService, searchService) {

    $scope.closeModal = function() {
      modalService.closeModal();
    };

    $scope.search = function() {
      $scope.searchResults = '';
      startSearch($scope.searchQuery);
    };

    var startSearch = ionic.debounce(function(query) {
      searchService.search(query)
        .then(function(data) {
          $scope.searchResults = data;
        });
    }, 750);

    $scope.goToStock = function(ticker) {
      modalService.closeModal();
      $state.go('app.stock', {stockTicker: ticker});
    };

}])

.controller('MyStocksCtrl', ['$scope','myStocksArrayService','stockDataService','stockDetailsCacheService','followStockService','stockPriceCacheService','comIDService',
  function($scope, myStocksArrayService,stockDataService,stockDetailsCacheService,followStockService,stockPriceCacheService,comIDService) {

    $scope.$on("$ionicView.afterEnter", function(){
      comIDService.populateCache();
      $scope.getMyStocksData();

    });

    $scope.getMyStocksData = function(){
      myStocksArrayService.forEach(function(stock){
        // console.log(stock);
        var promise = stockDataService.getDetailsData(stock.ticker);
        var promise2 = stockDataService.getPriceData(stock.ticker);
        $scope.myStocksData = [];
        $scope.myStocksData2 = [];

        promise.then(function(data){
          // console.log(data);
          $scope.myStocksData.push(stockDetailsCacheService.get(data.securitySymbol));

        });

        promise2.then(function(data){
          // console.log($scope.myStocksData);

          var jsonData = data.stock[0];
          var stockPriceInfo = stockPriceCacheService.get(jsonData.symbol);

          // console.log(stockPriceInfo);
          for (var i = 0; i < $scope.myStocksData.length; i++) {
            if ($scope.myStocksData[i].securitySymbol == jsonData.symbol) {
              $scope.myStocksData[i].dataAsOf = stockPriceInfo.as_of;
              $scope.myStocksData[i].securityName = stockPriceInfo.stock[0].name;
              // console.log($scope.myStocksData[i]);
              break;
            }
          }
        });
      });

      $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.unfollowStock = function(ticker){
      followStockService.unfollow(ticker);
      $scope.getMyStocksData();
    };

    $scope.getStockName = function(ticker){
      return $scope.myStocksData2[ticker];
    };

}])

.controller('StockCtrl', ['$scope',
                          '$stateParams',
                          '$window',
                          '$ionicPopup',
                          '$cordovaInAppBrowser',
                          'followStockService',
                          'stockDataService',
                          'dateService',
                          'chartDataService',
                          'notesService',
                          'newsService',
                          'stockPriceCacheService',
                          'comIDService',
                          function($scope,
                                  $stateParams,
                                  $window,
                                  $ionicPopup,
                                  $cordovaInAppBrowser,
                                  followStockService ,
                                  stockDataService,
                                  dateService,
                                  chartDataService,
                                  notesService,
                                  newsService,
                                  stockPriceCacheService,
                                  comIDService) {

    $scope.ticker = $stateParams.stockTicker;
    $scope.tickerComId = $stateParams.stockTicker;
    $scope.tickerSecId = $stateParams.SecId;

    $scope.chartView = 4;
    $scope.oneYearAgoDate = dateService.oneYearAgoDate();
    $scope.todayDate = dateService.currentDate();

    $scope.following = followStockService.checkFollowing($scope.ticker);
    $scope.stockNotes = [];


    $scope.$on("$ionicView.afterEnter", function(){
      comIDService.populateCache();
      getPriceData();
      getDetailsData();
      getChartData();
      $scope.stockNotes = notesService.getNotes($scope.ticker);
    });

    $scope.toggleFollow  = function(){
      if($scope.following){
        followStockService.unfollow($scope.ticker);
        $scope.following = false;
      }else{
        followStockService.follow($scope.ticker);
        $scope.following = true;
      }
    };

    $scope.openWindow = function(link){
      var inAppBrowserOptions = {
        location: 'yes',
        clearcache: 'yes',
        toolbar: 'yes'
      };

      $cordovaInAppBrowser.open(link, '_blank', inAppBrowserOptions);
    };

    $scope.addNote = function() {
      $scope.note = {title: 'Note', body: '', date: $scope.todayDate, ticker: $scope.ticker};

      var note = $ionicPopup.show({
        template: '<input type="text" ng-model="note.title" id="stock-note-title"><textarea type="text" ng-model="note.body" id="stock-note-body"></textarea>',
        title: 'New Note for ' + $scope.ticker,
        scope: $scope,
        buttons: [
          {
            text: 'Cancel',
            onTap: function(e) {
              return;
            }
          },
          {
            text: '<b>Save</b>',
            type: 'button-balanced',
            onTap: function(e) {
              notesService.addNote($scope.ticker, $scope.note);
              // console.log("save: ", $scope.note);
            }
          }
        ]
      });
      note.then(function(res) {
        $scope.stockNotes = notesService.getNotes($scope.ticker);
      });
    };

    $scope.openNote = function(index, title, body) {
      $scope.note = {title: title, body: body, date: $scope.todayDate, ticker: $scope.ticker};

      var note = $ionicPopup.show({
        template: '<input type="text" ng-model="note.title" id="stock-note-title"><textarea type="text" ng-model="note.body" id="stock-note-body"></textarea>',
        title: $scope.note.title,
        scope: $scope,
        buttons: [
          {
            text: 'Delete',
            type: 'button-assertive button-small',
            onTap: function(e) {
              notesService.deleteNote($scope.ticker, index);
            }
          },
          {
            text: 'Cancel',
            type: 'button-small',
            onTap: function(e) {
              return;
            }
          },
          {
            text: '<b>Save</b>',
            type: 'button-balanced button-small',
            onTap: function(e) {
              notesService.deleteNote($scope.ticker, index);
              notesService.addNote($scope.ticker, $scope.note);
              // console.log("save: ", $scope.note);
            }
          }
        ]
      });
      note.then(function(res) {
        $scope.stockNotes = notesService.getNotes($scope.ticker);
      });
    };

    function cleanText(st) {
      var s = st.replace(/<(?:.|\n)*?>/gm, '').replace(/&#([^\s]*);/g, function(x, y) { return String.fromCharCode(y); });
      return s;
    }

    function getNews(){

      $scope.newsStories = [];

      var promise = newsService.getNews($scope.stockPriceData.name);

      promise.then(function(data){
        $scope.newsStories = data;

        for (var i = 0; i < data.length; i++) {
          data[i].description = cleanText(data[i].description);
        }
        // console.log(data);
      });
    }

    function getPriceData(){
      var promise = stockDataService.getPriceData($scope.ticker);
      promise.then(function(data){
        $scope.stockPriceData = data.stock[0];
        $scope.dataAsOf = data.as_of;

        // get news after we have the name of stock already
        getNews();

        if(data.stock[0].percent_change >= 0 && data !== null){
          $scope.reactiveColor = {'background-color': '#33cd5f', 'border-color': 'rgba(255,255,255,0.3)'};
        }
        else if (data.stock[0].percent_change < 0 && data !== null) {
          $scope.reactiveColor = {'background-color' : '#ef473a', 'border-color': 'rgba(0,0,0,0.2)'};
        }

      });
    }

    function getDetailsData(){
      var promise = stockDataService.getDetailsData($scope.ticker);
      promise.then(function(data){
        $scope.stockDetailsData = data;

        $scope.totalVolume = data.headerTotalVolume.replace(/[^\d\.\-\ ]/g, '');
        $scope.totalValue = data.headerTotalValue.replace(/[^\d\.\-\ ]/g, '');
        $scope.lastTradedDate = data.lastTradedDate.split(" ")[0];

      });
    }

    function getChartData() {
      var promise = chartDataService.getHistoricalData($scope.ticker, $scope.oneYearAgoDate, $scope.todayDate);

      promise.then(function(data) {

        $scope.myData = JSON.parse(data)
                      	.map(function(series) {
                      		series.values = series.values.map(function(d) { return {x: d[0], y: d[1] }; });
                          return series;
                      	});
      });
    }

    var marginBottom = ($window.innerWidth / 100) * 10;

  	var xTickFormat = function(d) {
  		var dx = $scope.myData[0].values[d] && $scope.myData[0].values[d].x || 0;
  		if (dx > 0) {
        return d3.time.format("%b %d")(new Date(dx));
  		}
  		return null;
  	};

    var x2TickFormat = function(d) {
      var dx = $scope.myData[0].values[d] && $scope.myData[0].values[d].x || 0;
      return d3.time.format('%b %Y')(new Date(dx));
    };

    var y1TickFormat = function(d) {
      return d3.format(',f')(d);
    };

    var y2TickFormat = function(d) {
      return d3.format('s')(d);
    };

    var y3TickFormat = function(d) {
      return d3.format(',.2s')(d);
    };

    var y4TickFormat = function(d) {
      return d3.format(',.2s')(d);
    };

    var xValueFunction = function(d, i) {
      return i;
    };

  	$scope.chartOptions = {
      chartType: 'linePlusBarChart',
      data: 'myData',
      color: d3.scale.category10().range(),
      margin: {top: 25, right: 5, bottom: marginBottom, left: 5},
      interpolate: "cardinal",
      useInteractiveGuideline: false,
      yShowMaxMin: false,
      tooltips: false,
      showLegend: false,
      useVoronoi: false,
      xShowMaxMin: false,
      xValue: xValueFunction,
      xAxisTickFormat: xTickFormat,
      x2AxisTickFormat: x2TickFormat,
      y1AxisTickFormat: y1TickFormat,
      y2AxisTickFormat: y2TickFormat,
      y3AxisTickFormat: y3TickFormat,
      y4AxisTickFormat: y4TickFormat,
      transitionDuration: 500,
      y1AxisLabel: 'Price',
      y3AxisLabel: 'Volume',
      noData: 'Loading data...'
  	};


}])

.controller('LoginSignupCtrl', ['$scope', 'modalService','userService',
  function($scope, modalService, userService) {

    $scope.user = {email: '', password: ''};

    $scope.closeModal = function() {
      modalService.closeModal();
    };

    $scope.signup = function(user) {
      userService.signup(user);
    };

    $scope.login = function(user) {
      userService.login(user);
    };

  }])

;
